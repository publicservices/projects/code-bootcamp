#+TITLE: Javascript (js)
#+AUTHOR: i4k
#+OPTIONS: html-style:nil
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../assets/styles.css" />

* Where to execute some js code

- in your browser developer tool; on every page, right click > inspect page > =console=
- on https://jsbin.com/
- in a [[https://nodejs.dev/][node.js]] environement; after having it install, run `node` in your terminal.

* First javascript expressions

The following lines are each, a valid javascript expression.
Where you can write js, write one of them to see what it does.

#+begin_src js
		1 + 1
		'hello' + 'world'
		typeof true
		typeof 40 + 2
		typeof console.log
#+end_src

> An =expression=, is a snippet of (javascript) code that evaluates to a value.

* Variables

To store some javascript code, and expression, we can use a =variable=.

=var=,
=[[https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/let][let]]=,
=[[https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/const][const]]=,
are three different ways to "declare" (create) a new javascript
variable:

#+begin_src js
		// new ways of declaring a var
		let b = 2 + 2
		const c = 3 + 3

		// old way of declaring a variable
		var a = 1 + 1
#+end_src

> to "call" the variables (a, b and c), just write the varible name:
=a=, =b= or =c= (and press enter, to submit it to the js interpreter);
it should return your their values.

> Alternativelly, you can also use the =console.log= function, to log
anything (and also a variable) to the console: =console.log(a)= (in
the case you already have a variable named =a=).

- =const=, (as in: constant) is used for variables which type
  (boolean, object, string, etc.) should not change.
- =let=, declares a local variable.

> Note 1: no longer use =var=;


#+begin_src js
		let myVar = 'hello'
		myVar // (press "enter") this should give you the value of myVar
#+end_src

* functions

To encapsulate some sort of logic, we can use functions;

To declare a new function;

#+begin_src js
		const scream = (text) => {
						console.log(text + '!!!!!!!!!!!!!!!!')
		}

		scream('what is this')
		scream(42)
#+end_src

There are different ways to write a function:

#+begin_src js
		// a "named function"
		function sayHello(name) {
						console.log(name)
		}

		// the exact same function
		// but now it gets its name from the variable it is assigned to
		const sayHello = function(name) {
						console.log(name)
		}

		// the same function,
		// but now it is not a "named function",
		// but an "anonymous" arrow function,
		// that gets its name from the variable it is assigned to
		const sayHello = (name) => {
						console.log(name)
		}

		// all three versions can be called like this
		sayHello('Oprah Winfrey')

		// note: you would only choose one of these ways to declare the
		// function "sayHello", not all 3 as a function (and a variable) can
		// (and should) only be declared once
#+end_src

* types

There are different [[https://javascript.info/types][data types]] in javascript, from which the
principals are:

- string: =''=, =""=, =``= are three diffent way to write an empty string
- number: =1=, =2=, =33=, =444=, =5.4=, =3.14= ...
- boolean: =true= or =false=
- object: ={}= is an empty object
- undefined: for unassigned values (not yet assigned to a value)
- null: for unkown value (for which we don't know the value)

You can use the function =typeof=, to determine the type of something;

#+begin_src js
		typeof 22
		typeof 'hello'
		typeof console.log
		typeof {}
#+end_src

Let's look at a few types in more details.

** strings

Examples of strings:

#+begin_src js
		'hello world'
		'hello' + ' ' + 'myself' // will "concatenate" 3 strings together in one
		'32' // is a string, not a number
		'false' // is also a string, not a boolean
		'hello world'.toUpperCase() // calls the "toUpperCase" function, available on every strings
#+end_src

** objects

An object is a type that can be used to store data;

#+begin_src js
		// an empty object
		{}

		// an object, with 3 keys: size, name, isHuman
		{
						size: 3,
						name: 'aicha',
						isHuman: true
		}
#+end_src

An object, can be empty, or it can have (an unlimited number) of "key and value" pairs.

In the object assigned to the variable =userExample=:
- the 3 keys keys are: =name=, =isOnline=, =isPremium=, and their
  respective values are, ='bob'=, =false=, =undefined=.

#+begin_src
		// an object stored in a variable
		let userExample = {
						name: 'bob',
						isOnline: false,
						isPremium: undefined
		}
#+end_src

To access the value of an object's key:

#+begin_src js
		// the "dot" notation
		userExample.name // will give: 'bob'

		// the "bracket" notation
		userExample[name] // will give: 'bob'
#+end_src

To update an object's key, or create a new one:

#+begin_src js
		// the "dot" notation
		userExample.name = 'alice'

		// the "bracket" notation
		userExample[name] = 'stevie'

		// now try to retrieve the value of the "userExample" object "name" key
#+end_src

** arrays

Arrays are not a javascript default "type", but they exist!

An array is a list of things:

#+begin_src js
		// an empty array
		let myEmptyArray = []

		// an array with two elements (numbers)
		let myArray = [42, 88]

		// an array with a multitude of elements with different types
		let anOtherArray = [true, 22, 'hello', 'what is this???', {}]
#+end_src


It is possible manipulate, add and remove elements arrays;

#+begin_src js
		// let's make a new empty array, and save it to a variable
		let aNewArray = []

		// let's add some element inside this new array
		aNewArray.push('hello') // pushing a string inside the empty array
		aNewArray.push('world') // pushing an other string inside the array
		aNewArray.push(false) // pushing the boolean false
		aNewArray.push({}) // pushing an empty object
		// now the array should have 4 elements

		// let's see what is inside now
		console.log(aNewArray)

		// we can also remove elements
		aNewArray.pop(0) // removing the element at index 0 (which is the first element in the list)
		console.log(aNewArray)

		// if we want to read the value of an element in the array (without removing it)
		aNewArray[0] // where 0 is the element index (its position in the array, the first being 0)
#+end_src

It is possible to know how many elements are in an array:

#+begin_src js
		[].length // is 0, because there is no element in this empty array
		[1,2,3,4].length // how many elements?
		let myArray = ['a', 'b', 'c', 'd', 'e', 'f']
		myArray.length // to know how many elements are inside
#+end_src
